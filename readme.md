# web-obj-validator
Validator for javascript object (Web version)

## What's New

### v1.1.0
* Add new rules: **email**, **logic**


## Install
```bash
npm install web-obj-validator
```

## Include
```html
<script src="node_modules/web-module/web-module.js"/>
<script src="node_modules/web-obj-validator/web-obj-validator.js"/>
```

## Usage
```javascript
const Validator = module.require('web-obj-validator');

let validator = new Validator(validation_rule);

let rs = validator.validate(obj, (errors)=>{
  // Do something on error
}, ()=>{
  // Do something on ok
});
```

## Example
```javascript
let validator = new Validator({
  required: true,
  typeof  : 'object',
  child   : {
    name  : {
      required: true,
      typeof  : 'string',
      length  : {
        max    : 32
        message: 'Name must be less than or equal 32 characters'
      }
    },
    age   : {
      required: true,
      typeof  : 'number',
      value   : {
        min   : 1,
        max   : 100,
        message : {
          min   : 'Age must be greater than or equal 1',
          max   : 'Age must be less then or equal 100'
        }
      }
    }
  }
});

let rs = validator.validate({
  name: 'John',
  age : 32
});
```

## Methods

### .validate(*obj*, *onErrorCallback*, *onOkCallback*)

#### Parameters:

* **obj** : ***object*** Object to be validated

* **onErrorCallback** : ***function*** (*optional*) function(*errors*)

  * **errors** : ***Array***

    [ { id: *obj.subobj* , value: *object value* , message: *error message* } , ... ]


* **onOkCallback** : ***function*** (*optional*) function()

* ***return*** : ***boolean*** **true** if validation is ok

## Rules

### required
Validate if the field value is not values listed as below:
* **undefined**
* **null**
* empty array ( [ ] )
* empty string ( '' )


```javascript
{
  required: true // or false
}
```
```javascript
{
  required: 'Error message' // Equivalent to { required: true } with custom error message
}
```
```javascript
{
  required: {
    message: 'Error message' // Custom message
  }
}
```

### typeof
Validate the data type of field

```javascript
{
  typeof: 'some type' // undefined | string | number | object | function
}
```
```javascript
{
  typeof: [ 'type 1', 'type 2' ]
}
```
```javascript
{
  typeof: {
    types: 'some type'
  }
}
```
```javascript
{
  typeof: {
    types: [ 'type 1', 'type 2' ]
  }
}
```
```javascript
{
  typeof: {
    types: 'some type',
    message: 'Error message' // Custom error message
  }
}
```

### length
Validate string length
```javascript
{
  length: 8 // or '8'. Fixed length (8 characters)
}
```
```javascript
{
  length: {
    length: 8 // or '8'. Fixed length (8 characters)
  }
}
```
```javascript
{
  length: {
    min: 8 // or '8'. Mininum length (8 characters)
  }
}
```
```javascript
{
  length: {
    max: 8 // or '8'. Maxinum length (8 characters)
  }
}
```
```javascript
{
  length: {
    length: 8,
    message: 'Error message' // Custom error message (apply for all fixed length, minimum length, maximum length error)
  }
}
```
```javascript
{
  length: {
    length: 8,
    message: {
      length: 'Error message' // Custom error message (apply for fixed length error)
    }
  }
}
```
```javascript
{
  length: {
    min: 8,
    max: 16,
    message: {
      min: 'Error message 1', // Custom error message (apply for minimun length error)
      max: 'Error message 2' // Custom error message (apply for maxinum length error)
    }
  }
}
```

### value
Validate the value of string or number
```javascript
{
  value: 'string' // or number. Fixed value
}
```
```javascript
{
  value: [ value_1, value_2, ... ]
}
```
```javascript
{
  value: {
    min: 'string', // or number. Minimum value
  }
}
```
```javascript
{
  value: {
    max: 'string', // or number. Maximum value
  }
}
```
```javascript
{
  value: {
    value: 'string', // or number. Fixed value
  }
}
```
```javascript
{
  value: {
    min: 18,
    max: 100,
    message: 'Error message' // Custom error message (apply for all fixed value, mininum value, maxinum value)
  }
}
```
```javascript
{
  value: {
    value: [1, 2, 3],
    message: {
      value: 'Error message' // Custom error message (apply for fixed value)
    }
  }
}
```
```javascript
{
  value: {
    min: 50,
    max: 100,
    message: {
      min: 'Error message 1', // Custom error message (apply for minimum value)
      max: 'Error message 2' // Custom error message (apply for maxinum value)
    }
  }
}
```

### match
Validate if the string matchs specified pattern
```javascript
{
  match: 'pattern' // String
}
```
```javascript
{
  match: /^\d+$/ // or new RegExp('pattern'). Regular expression object
}
```
```javascript
{
  match: {
    pattern: 'pattern' // String
  }
}
```
```javascript
{
  match: {
    pattern: new RegExp('^\\d+$'),
    message: 'Error message' // Custom error message
  }
}
```

### email
Validate the email address format.
The email address format is validated by the regular expression below (from [http://emailregex.com](http://emailregex.com)):
```javascript
/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
```
```javascript
{
  email: true
}
```
```javascript
{
  email: 'Error message' // Custom error message
}
```
```javascript
{
  email: {
    message: 'Error message' // Custom error message
  }
}
```

### logic
Validate the custom logic
```javascript
{
  logic: function(value){ // Some logic. Return true | false }
}
```
```javascript
{
  logic: {
    logic: function(value){ // Some logic. Return true | false },
    message: 'Error message' // Custom error message
  }
}
```
```javascript
{
  logic: {
    logic: function(value){ // Some logic. Return true | false },
    context: obj, // Custom context
    message: 'Error message' // Custom error message
  }
}
```

### child
Validate specified chillren fields
```javascript
{
  child: {
    field_1: {
      ... // rules for field_1
    },
    field_2: {
      ... // rules for field_2
    },
    ...
  }
}
```

### children
Validate all children fields
```javascript
{
  children: {
    ... // rules for all children fields
  }
}
```

## Callbacks

### on_ok
```javascript
let validator = new Validator({
  required: true,
  typeof  : 'object',
  child   : {
    name  : {
      required: {
        message: 'Name is required',
        on_ok  : (id, value)=>{
          console.log(id); // name
          console.log(value); // undefined
        }
      },
      length: {
        max: 32
      }
    }
  }
});

validator.validate({
  name: 'John'
});
```

### on_error
```javascript
let validator = new Validator({
  required: true,
  typeof  : 'object',
  child   : {
    name  : {
      required: {
        message: 'Name is required',
        on_error  : (id, value, message)=>{
          console.log(id); // name
          console.log(value); // undefined
          console.log(message); // Name is required
        }
      },
      length: {
        max: 32
      }
    }
  }
});

validator.validate({
  name: undefined
});
```

## Bugs or feature requests
Please contact to [email](mailto:lchild358@yahoo.com.vn)

## License
[ISC](https://opensource.org/licenses/ISC)

