module.define('web-obj-validator', (function(){

  /** Utility functions */
  const Util = {};

  /**
   * Convenient method
   * Check if the given object is null object
   * @param obj mixed
   * @return boolean
   */
  Util.isNullObject = function(obj){
    return typeof(obj) === 'object' && obj === null;
  };

  /**
   * Convenient method
   * Check if the given object is an object that is not null
   * @param obj mixed
   * @return boolean
   */
  Util.isNotNullObject = function(obj){
    return typeof(obj) === 'object' && obj !== null;
  };

  /**
   * Convenient method
   * Check if the given object is an empty string
   * @param obj mixed
   * @return boolean
   */
  Util.isEmptyString = function(obj){
    return typeof(obj) === 'string' && obj === '';
  };

  /**
   * Convenient method
   * Check if the given object is a string that is not empty
   * @param obj mixed
   * @return boolean
   */
  Util.isNotEmptyString = function(obj){
    return typeof(obj) === 'string' && obj !== '';
  };
  /**
   * Convenient method
   * Check if the given object is an empty array
   * @param obj mixed
   * @return boolean
   */
  Util.isEmptyArray = function(obj){
    return typeof(obj) === 'object' && (obj instanceof Array) && obj.length <= 0;
  };

  /**
   * Convenient method
   * Check if the given object is an array that not empty
   * @param obj mixed
   * @return boolean
   */
  Util.isNotEmptyArray = function(obj){
    return typeof(obj) === 'object' && (obj instanceof Array) && obj.length > 0;
  };

  /**
   * Convenient method
   * Check if the given object is an integer string
   * @param obj mixed
   * @return boolean
   */
  Util.isIntegerString = function(obj){
    return typeof(obj) === 'string' && /^-?\d+$/.test(obj);
  };

  /**
   * Convenient method
   * Check if the given object is an positive integer string
   * @param obj mixed
   * @return boolean
   */
  Util.isPositiveIntegerString = function(obj){
    return typeof(obj) === 'string' && /^\d+$/.test(obj);
  };
  /**
   * Convenient method
   * Check if the given object is an negaitive integer string
   * @param obj mixed
   * @return boolean
   */
  Util.isNegativeIntegerString = function(obj){
    return typeof(obj) === 'string' && /^-\d+$/.test(obj);
  };

  /**
   * Convenient method
   * Check if the given object is an float string
   * @param obj mixed
   * @return boolean
   */
  Util.isFloatString = function(obj){
    return typeof(obj) === 'string' && /^-?\d+\.\d+$/.test(obj);
  };

  /**
   * Convenient method
   * Check if the given object is an positive float string
   * @param obj mixed
   * @return boolean
   */
  Util.isPositiveFloatString = function(obj){
    return typeof(obj) === 'string' && /^\d+\.\d+$/.test(obj);
  };

  /**
   * Convenient method
   * Check if the given object is an negative float string
   * @param obj mixed
   * @return boolean
   */
  Util.isNegativeFloatString = function(obj){
    return typeof(obj) === 'string' && /^-\d+\.\d+$/.test(obj);
  };
  /**
   * Convenient method
   * Check if the given object is a positive number
   * @param obj mixed
   * @return boolean
   */
  Util.isPositiveNumber = function(obj){
    return typeof(obj) === 'number' && obj >= 0;
  };

  /**
   * Convenient method
   * Check if the given object is a negative number
   * @param obj mixed
   * @return boolean
   */
  Util.isNegativeNumber = function(obj){
    return typeof(obj) === 'number' && obj < 0;
  };

  Util.isIntegerNumber = function(obj){
    return typeof(obj) === 'number' && Number.isInteger(obj);
  };

  Util.isPositiveInteger = function(obj){
    return typeof(obj) === 'number' && Number.isInteger(obj) && obj >= 0;
  };

  Util.isNegativeInteger = function(obj){
    return typeof(obj) === 'number' && Number.isInteger(obj) && obj < 0;
  };

  /******************************************************************************************/

  const Filter = function(){
    this._rules = [];
  };

  Filter.prototype.add = function(cond, exec){
    this._rules[this._rules.length] = {cond: cond, exec: exec};
    return this;
  };

  Filter.prototype.apply = function(value){
    for(var rule of this._rules){
      if(rule.cond(value)) return rule.exec(value);
    }
    return value;
  };

  /**
   * Filters
   */
  const Filters = function(value){

    while(typeof value === 'function'){
      value = value();
    }

    this._value = value;
  };

  /**
   *
   */
  Filters.prototype.get = function(){
    return this._value;
  };

  /**
   *
   */
  Filters.prototype.string = function(def){
    let f = new Filter();

    this._value = f.add(
      (v)=>{ return v === undefined || v === null; },
      (v)=>{ return typeof(def) === 'string' ? def : ''; }
    ).add(
      (v)=>{ return typeof(v) === 'boolean'; },
      (v)=>{ return v ? 'true' : 'false'; }
    ).add(
      (v)=>{ return typeof(v) === 'number' || typeof(v) === 'object'; },
      (v)=>{ return v.toString(); }
    ).apply(this._value);

    return this;
  };

  /**
   *
   */
  Filters.prototype.trim = function(){
    let f = new Filter();

    if(arguments.length == 1){
      if(typeof arguments[0] === 'string'){
        this._value = f.add(
          (v)=>{ return typeof(v) === 'string'; },
          (v)=>{ return v.replace(new RegExp(`^${arguments[0]}|${arguments[0]}$`, 'gm'), ''); }
        ).apply(this._value);
      }else if(typeof(arguments[0]) === 'object' && (arguments[0] instanceof RegExp)){
        this._value = f.add(
          (v)=>{ return typeof(v) === 'string'; },
          (v)=>{ return v.replace(arguments[0], ''); }
        ).apply(this._value);
      }
    }else if(arguments.length == 2){
      if(typeof(arguments[0]) === 'string' && typeof(arguments[1]) === 'string'){
        this._value = f.add(
          (v)=>{ return typeof(v) === 'string'; },
          (v)=>{ return v.replace(new RegExp(`^${arguments[0]}|${arguments[1]}$`, 'gm'), ''); }
        ).apply(this._value);
      }
    }else{
      this._value = f.add(
        (v)=>{ return typeof(v) === 'string'; },
        (v)=>{ return v.trim(); }
      ).apply(this._value);
    }

    return this;
  };

  /**
   *
   */
  Filters.prototype.number = function(def){
    let f = new Filter();

    this._value = f.add(
      (v)=>{ return v === undefined || v === null; },
      (v)=>{ return typeof(def) === 'number' ? def : 0; }
    ).add(
      (v)=>{ return typeof v === 'boolean'; },
      (v)=>{ return v ? 1 : 0; }
    ).add(
      (v)=>{ return typeof(v) === 'string' && v.trim() == ''; },
      (v)=>{ return typeof(def) === 'number' ? def : 0; }
    ).add(
      (v)=>{ return typeof v === 'string'; },
      (v)=>{ var tmp = new Number(v); return isNaN(tmp) ? (typeof def === 'number' ? def : 0) : tmp.valueOf(); }
    ).add(
      (v)=>{ return typeof(v) === 'object' && v.toString().trim() == '' },
      (v)=>{ return typeof(def) === 'number' ? def : 0; }
    ).add(
      (v)=>{ return typeof v === 'object'; },
      (v)=>{ var tmp = new Number(v.toString()); return isNaN(tmp) ? (typeof def === 'number' ? def : 0) : tmp.valueOf(); }
    ).apply(this._value);

    return this;
  };

  /**
   *
   */
  Filters.prototype.int = function(radix, def){
  
    let f = new Filter();

    this._value = f.add(
      (v)=>{ return v === undefined || v === null; },
      (v)=>{ return typeof(def) === 'number' ? (Number.isInteger(def) ? def : Math.trunc(def)) : 0; }
    ).add(
      (v)=>{ return typeof v === 'boolean'; },
      (v)=>{ return v ? 1 : 0; }
    ).add(
      (v)=>{ return typeof v === 'number'; },
      (v)=>{ return Number.isInteger(v) ? v : Math.trunc(v); }
    ).add(
      (v)=>{ return typeof(v) === 'string' && v.trim() == ''; },
      (v)=>{ return typeof(def) === 'number' ? (Number.isInteger(def) ? def : Math.trunc(def)) : 0; }
    ).add(
      (v)=>{ return typeof v === 'string'; },
      (v)=>{ 
        var tmp = parseInt(v, Number.isInteger(radix) ? radix : 10);
        return isNaN(tmp) ? (typeof def === 'number' ? (Number.isInteger(def) ? def : Math.trunc(def)) : 0) : tmp; }
    ).add(
      (v)=>{ return typeof v === 'object'; },
      (v)=>{ 
        var tmp = parseInt(v.toString(), Number.isInteger(radix) ? radix : 10);
        return isNaN(tmp) ? (typeof def === 'number' ? (Number.isInteger(def) ? def : Math.trunc(def)) : 0) : tmp; }
    ).apply(this._value);

    return this;
  };

  /**
   *
   */
  Filters.prototype.float = function(def){
    let f = new Filter();

    this._value = f.add(
      (v)=>{ return v === undefined || v === null; },
      (v)=>{ return typeof(def) === 'number' ? def : 0; }
    ).add(
      (v)=>{ return typeof v === 'boolean'; },
      (v)=>{ return v ? 1 : 0; }
    ).add(
      (v)=>{ return typeof v === 'string'; },
      (v)=>{
        var tmp = parseFloat(v);
        return isNaN(tmp) ? (typeof def === 'number' ? def : 0) : tmp; }
    ).add(
      (v)=>{ return typeof v === 'object'; },
      (v)=>{
        var tmp = parseFloat(v.toString());
        return isNaN(tmp) ? (typeof def === 'number' ? def : 0) : tmp; }
    ).apply(this._value);

    return this;
  };

  /**
   * Custom filter
   */
  Filters.prototype.custom = function(f, context){
    if(typeof f === 'function') this._value = f.call(context ? context : this, this._value);
    return this;
  };

  /**
   * 
   */
  const filter = function(value){
    return new Filters(value);
  };

  /**
   * Bind
   */
  filter.bind = function(name, f, context){
    Filters.prototype[name] = function(){
      Array.prototype.splice.call(arguments, 0, 0, this._value);
      this._value = f.apply(context ? context : this, arguments);
      return this;
    };
    return retf;
  };

  /******************************************************************************************/
  
  const format = function(text, values){
    if(values === undefined || values === null){
      return text.replace(/\{0\}/g, '');
    }

    switch(typeof(values)){
    case 'string':
    case 'number':
      return text.replace(/\{0\}/g, values);
      break;
    case 'boolean':
      return text.replace(/\{0\}/g, values ? 'true' : 'false');
      break;
    case 'object':

      for(let name in values){
        let value = values[name];
        if(value === undefined || value === null){
          text = text.replace(/\{0\}/g, '');
        }else{
          switch(typeof(value)){
          case 'string':
          case 'number':
            text = text.replace(/\{0\}/g, value);
            break;
          case 'boolean':
            text = text.replace(/\{0\}/g, value ? 'true' : 'false');
            break;
          }
        }
      }

      return text;
      break;
    default:
      return text;
    }
  };

  /******************************************************************************************/

  /**
   * CommonRule class
   */
  const CommonRule = function(){ };

  CommonRule.prototype.contructor = CommonRule;

  /**
   * @param f Function
   */
  CommonRule.prototype.addOnError = function(f){
    if(typeof f === 'function'){
      if(! this.onerrs){
        this.onerrs = [f];
      }else{
        this.onerrs[this.onerrs.length] = f;
      }
    }
  };

  /**
   * @param f Function
   */
  CommonRule.prototype.addOnOk = function(f){
    if(typeof f === 'function'){
      if(! this.onoks){
        this.onoks = [f];
      }else{
        this.onoks[this.onoks.length] = f;
      }
    }
  };

  /**
   *
   */
  CommonRule.prototype.getOnErrors = function(){
    if(this.onerrs){
      return this.onerrs;
    }else{
      return [];
    }
  };

  /**
   *
   */
  CommonRule.prototype.getOnOks = function(){
    if(this.onoks){
      return this.onoks;
    }else{
      return [];
    }
  };

  /**
   *
   */
  CommonRule.prototype.onError = function(){
    var message = this.getMessage();

    var validateContext = this.getValidateContext();

    var validateId = this.getValidateId();

    var validateObj = this.getValidateObj();

    var fs = this.getOnErrors();
    for(var f of fs){
      f(validateId, validateObj, message);
    }

    if(typeof this.validateOnError === 'function'){
      this.validateOnError.call(validateContext, validateId, validateObj, message);
    }

    return false;
  };

  /**
   *
   */
  CommonRule.prototype.onOk = function(){
    var validateContext = this.getValidateContext();

    var validateId = this.getValidateId();

    var validateObj = this.getValidateObj();

    var fs = this.getOnOks();
    for(var f of fs){
      f(validateId, validateObj);
    }

    if(typeof this.validateOnOk === 'function'){
      this.validateOnOk.call(validateContext, validateId, validateObj);
    }

    return true;
  };

  /**
   * @param options
   */
  CommonRule.prototype.init = function(options){
    if(typeof(options) === 'object' && options !== null){
      if(typeof options.on_error === 'function'){
        this.addOnError(options.on_error);
      }
      if(typeof options.on_ok === 'function'){
        this.addOnOk(options.on_ok);
      }
    }
  };

  /**
   *
   */
  CommonRule.prototype.getMessage = function(){
    // TODO Override
    return undefined;
  };

  CommonRule.prototype.getValidateContext = function(){
    // TODO Override
    return this.validateContext;
  };

  CommonRule.prototype.getValidateId = function(){
    // TODO Override
    return this.validateId;
  };

  CommonRule.prototype.getValidateObj = function(){
    // TODO Override
    return this.validateObj;
  };

  /**
   * @param id
   * @param obj
   * @param onError
   * @param onOk
   * @param context
   */
  CommonRule.prototype.initValidation = function(id, obj, onError, onOk, context){
    this.validateId      = id;
    this.validateObj     = obj;
    this.validateOnError = onError;
    this.validateOnOk    = onOk;
    this.validateContext = context;
  };

  CommonRule.valToStr = function(val){
    if(typeof val === 'undefined'){
      return 'UNDEFINED';
    }else if(typeof val === 'string'){
      if(val === ''){
        return 'EMPTY STRING';
      }else{
        return `"${val}"`;
      }
    }else if(typeof val === 'object'){
      if(val === null){
        return 'NULL';
      }else if(val instanceof Array){
        if(val.length <= 0){
          return 'EMPTY ARRAY';
        }else{
          return JSON.stringify(val);
        }
      }else{
        return JSON.stringify(val);
      }
    }else if(typeof val === 'boolean'){
      return val ? 'TRUE' : 'FALSE';
    }else if(typeof val === 'function'){
      return 'FUNCTION';
    }else{
      return val;
    }
  };

  /******************************************************************************************/

  /**
   * Required rule
   */
  const RequiredRule = function(){
    CommonRule.call(this);

    this.required = undefined;
    this.message = 'Validation failed: parameter is required';
  };

  /** Inheritance */
  RequiredRule.prototype = Object.create(CommonRule.prototype);
  RequiredRule.prototype.contructor = RequiredRule;

  /**
   * @param options
   */
  RequiredRule.parse = function(options){
    var ins = new RequiredRule();

    ins.init(options);

    if(typeof options === 'boolean'){
      // Required
      ins.required = options;
    }else if(Util.isNotEmptyString(options)){
      // Required
      ins.required = true;
      // Message
      ins.message = options;
    }else if(Util.isNotNullObject(options)){
      if(Util.isNotEmptyString(options.message)){
        // Required
        ins.required = true;
        // Message
        ins.message = options.message;
      }
    }

    if(typeof ins.required === 'undefined') throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`);

    return ins;
  };

  /**
   *
   */
  RequiredRule.prototype.getMessage = function(){
    return this.message;
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  RequiredRule.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(! this.required){
      return this.onOk();
    }
    if(typeof obj === 'undefined'){
      return this.onError();
    }
    if(Util.isNullObject(obj)){
      return this.onError();
    }
    if(Util.isEmptyArray(obj)){
      return this.onError();
    }
    if(Util.isEmptyString(obj)){
      return this.onError();
    }

    return this.onOk();
  };

  /******************************************************************************************/

  /**
   * TypeofRule class
   */
  const TypeofRule = function(){
    this.types = [];
    this.message = 'Validation failed: Unexpected data type. Expected: {expected}';
  }
  
  /** Prototype */
  TypeofRule.prototype = Object.create(CommonRule.prototype);
  TypeofRule.prototype.constructor = TypeofRule;

  /**
   * @param options
   */
  TypeofRule.parse = function(options){
    var ins = new TypeofRule();

    ins.init(options);

    const filter = (item)=>{
      return Util.isNotEmptyString(item);
    };

    if(Util.isNotEmptyString(options)){
      // Types (Single)
      ins.types = [options];
    }else if(Util.isNotEmptyArray(options)){
      // Types (Multiple)
      ins.types = options.filter(filter);
    }else if(Util.isNotNullObject(options)){
      // Types
      if(Util.isNotEmptyString(options.types)){
        // Single
        ins.types = [options.types];
      }else if(Util.isNotEmptyArray(options.types)){
        // Multiple
        ins.types = options.types.filter(filter);
      }
      // Message
      if(Util.isNotEmptyString(options.message)){
        ins.message = options.message;
      }
    }

    if(Util.isEmptyArray(ins.types)) throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 

    return ins;
  };

  /**
   *
   */
  TypeofRule.prototype.getMessage = function(){
    return format(this.message, {expected: CommonRule.valToStr(this.types)});
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  TypeofRule.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);
    
    const type = typeof(obj);

    if(this.types.indexOf(type) >= 0){
      return this.onOk();
    }

    return this.onError();
  };

  /******************************************************************************************/

  /**
   * LengthRule class
   */
  const LengthRule = function(){
    this.min            = -1;
    this.max            = -1;
    this.length         = -1;
    
    this.min_message    = 'Validation failed: Invalid length, expected at least {min} characters';
    this.max_message    = 'Validation failed: Invalid length, expected at most {max} characters';
    this.length_message = 'Validation failed: Invalid length, expected {length} characters';
    
    this.min_err_flg    = false;
    this.max_err_flg    = false;
    this.length_err_flg = false;
  }
  
  /** Prototype */
  LengthRule.prototype = Object.create(CommonRule.prototype);
  LengthRule.prototype.constructor = LengthRule;

  /**
   * @param options
   */
  LengthRule.parse = function(options){
    var ins = new LengthRule();

    ins.init(options);

    if(Util.isPositiveInteger(options)){
      // Length
      ins.length = options;
    }else if(Util.isPositiveIntegerString(options)){
      // Length
      ins.length = filter(options).int().get();
    }else if(Util.isNotNullObject(options) && ! (options instanceof Array)){
      // Min
      if(Util.isPositiveInteger(options.min)){
        ins.min = options.min;
      }else if(Util.isPositiveIntegerString(options.min)){
        ins.min = filter(options.min).int().get();
      }
      // Max
      if(Util.isPositiveInteger(options.max)){
        ins.max = options.max;
      }else if(Util.isPositiveIntegerString(options.max)){
        ins.max = filter(options.max).int().get();
      }
      // Length
      if(Util.isPositiveInteger(options.length)){
        ins.length = options.length;
      }else if(Util.isPositiveIntegerString(options.length)){
        ins.length = filter(options.length).int().get();
      }
      // Message
      if(Util.isNotEmptyString(options.message) && ! (options.message instanceof Array)){
        ins.min_message = ins.max_message = ins.length_message = options.message;
      }else if(Util.isNotNullObject(options.message)){
        // Min message
        if(Util.isNotEmptyString(options.message.min)){
          ins.min_message = options.message.min;
        }
        // Max message
        if(Util.isNotEmptyString(options.message.max)){
          ins.max_message = options.message.max;
        }
        // Length message
        if(Util.isNotEmptyString(options.message.length)){
          ins.length_message = options.message.length;
        }
      }
    }

    if(ins.min < 0 && ins.max < 0 && ins.length < 0) throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 

    return ins;
  };

  /**
   *
   */
  LengthRule.prototype.getMessage = function(){
    if(this.min_err_flg){
      return format(this.min_message, {min: CommonRule.valToStr(this.min)});
    }else if(this.max_err_flg){
      return format(this.max_message, {max: CommonRule.valToStr(this.max)});
    }else{
      return format(this.length_message, {length: CommonRule.valToStr(this.length)});
    }
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  LengthRule.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(typeof(obj) !== 'string') return this.onOk();

    let len = obj.length;

    if(this.min >= 0 && len < this.min){
      this.min_err_flg = true;
      return this.onError();
    }

    if(this.max >= 0 && len > this.max){
      this.max_err_flg = true;
      return this.onError();
    }

    if(this.length >= 0 && len != this.length){
      this.length_err_flg = true;
      return this.onError();
    }

    return this.onOk();
  };

  /******************************************************************************************/

  /**
   * ValueRule class
   */
  const ValueRule = function(){
    this.min            = undefined;
    this.max            = undefined;
    this.fix            = undefined;
    this.in             = [];
    
    this.min_message    = 'Validation failed: Invalid value, must be greater than or equals {min}';
    this.max_message    = 'Validation failed: Invalid value, must be less than or equals {max}';
    this.fix_message    = 'validation failed: Invalid value, must be {value}';
    this.in_message     = 'Validation failed: Invalid value, must be one of {values}';
    
    this.min_err_flg    = false;
    this.max_err_flg    = false;
    this.fix_err_flg    = false;
    this.in_err_flg     = false;
  }
  
  /** Prototype */
  ValueRule.prototype = Object.create(CommonRule.prototype);
  ValueRule.prototype.constructor = ValueRule;

  /**
   * @param options
   */
  ValueRule.parse = function(options){
    var ins = new ValueRule();

    ins.init(options);

    if(typeof options === 'string'){
      // Fix
      ins.fix = options;
    }else if(typeof options === 'number'){
      // Fix
      ins.fix = options;
    }else if(Util.isNotEmptyArray(options)){
      // In
      ins.in = options;
    }else if(Util.isNotNullObject(options)){
      // Min
      if(typeof options.min === 'number'){
        ins.min = options.min;
      }else if(typeof options.min === 'string'){
        ins.min = options.min;
      }
      // Max
      if(typeof options.max === 'number'){
        ins.max = options.max;
      }else if(typeof options.max === 'string'){
        ins.max = options.max;
      }
      // Value
      if(typeof options.value === 'string'){
        // Fix
        ins.fix = options.value;
      }else if(typeof options.value === 'number'){
        // Fix
        ins.fix = options.value;
      }else if(Util.isNotEmptyArray(options.value)){
        // In
        ins.in = options.value;
      }
      // Message
      if(Util.isNotEmptyString(options.message)){
        ins.min_message = ins.max_message = ins.fix_message = ins.in_message = options.message;
      }else if(Util.isNotNullObject(options.message)){
        if(Util.isNotEmptyString(options.message.min)){
          // Min
          ins.min_message = options.message.min;
        }else if(Util.isNotEmptyString(options.message.max)){a
          // Max
          ins.max_message = options.message.max;
        }else if(Util.isNotEmptyString(options.message.value)){
          // Fix, In
          ins.fix_message = ins.in_message = options.message.value;
        }
      }
    }

    if(ins.min === undefined && ins.max === undefined && ins.fix === undefined && ins.in.length <= 0){
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 
    }

    return ins;
  };

  /**
   *
   */
  ValueRule.prototype.getMessage = function(){
    if(this.min_err_flg){
      return format(this.min_message, {min: CommonRule.valToStr(this.min)});
    }else if(this.max_err_flg){
      return format(this.max_message, {max: CommonRule.valToStr(this.max)});
    }else if(this.fix_err_flg){
      return format(this.fix_message, {value: CommonRule.valToStr(this.fix)});
    }else{
      return format(this.in_message, {values: JSON.stringify(this.in)});
    }
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  ValueRule.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(typeof(obj) !== 'string' && typeof(obj) !== 'number') return this.onOk();

    // Min
    if(typeof this.min === 'string'){
      if(typeof obj === 'string'){
        if(obj < this.min){
          this.min_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'number'){
        if(filter(obj).string().get() < this.min){
          this.min_err_flg = true;
          return this.onError();
        }
      }
    }else if(typeof this.min === 'number'){
      if(typeof obj === 'number'){
        if(obj < this.min){
          this.min_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'string'){
        if(filter(obj).number().get() < this.min){
          this.min_err_flg = true;
          return this.onError();
        }
      }
    }
    // Max
    if(typeof this.max === 'string'){
      if(typeof obj === 'string'){
        if(obj > this.max){
          this.max_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'number'){
        if(filter(obj).string().get() > this.max){
          this.max_err_flg = true;
          return this.onError();
        }
      }
    }else if(typeof this.max === 'number'){
      if(typeof obj === 'number'){
        if(obj > this.max){
          this.max_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'string'){
        if(filter(obj).number().get() > this.max){
          this.max_err_flg = true;
          return this.onError();
        }
      }
    }
    // Fix
    if(typeof this.fix === 'string'){
      if(typeof obj === 'string'){
        if(obj !== this.fix){
          this.fix_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'number'){
        if(filter(obj).string().get() !== this.fix){
          this.fix_err_flg = true;
          return this.onError();
        }
      }
    }else if(typeof this.fix === 'number'){
      if(typeof obj === 'number'){
        if(obj !== this.fix){
          this.fix_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'string'){
        if(filter(obj).number().get() !== this.fix){
          this.fix_err_flg = true;
          return this.onError();
        }
      }
    }
    // In
    if(this.in.length > 0){
      for(var val of this.in){
        if(obj === val){
          return this.onOk();
        }
      }
      this.in_err_flg = true;
      return this.onError();
    }

    return this.onOk();
  };

  /******************************************************************************************/

  /**
   * ValueRule class
   */
  const MatchRule = function(){
    this.pattern    = undefined;
    
    this.message    = 'Validation failed: Invalid value, must match {pattern}';
  }
  
  /** Prototype */
  MatchRule.prototype = Object.create(CommonRule.prototype);
  MatchRule.prototype.constructor = MatchRule;

  /**
   * @param options
   */
  MatchRule.parse = function(options){
    var ins = new MatchRule();

    ins.init(options);

    if(Util.isNotEmptyString(options)){
      // Pattern
      ins.pattern = new RegExp(options);
    }else if(Util.isNotNullObject(options)){
      if(options instanceof RegExp){
        // Pattern
        ins.pattern = options;
      }else{
        // Pattern
        if(Util.isNotEmptyString(options.pattern)){
          ins.pattern = new RegExp(options.pattern);
        }else if(Util.isNotNullObject(options.pattern) && (options.pattern instanceof RegExp)){
          ins.pattern = options.pattern;
        }
        // Message
        if(Util.isNotEmptyString(options.message)){
          ins.message = options.message;
        }
      }
    }

    if(ins.pattern === undefined){
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 
    }

    return ins;
  };

  /**
   *
   */
  MatchRule.prototype.getMessage = function(){
      return format(this.message, {pattern: this.pattern.toString()});
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  MatchRule.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(typeof(obj) === 'string'){
      if(this.pattern.test(obj)){
        return this.onOk();
      }else{
        return this.onError();
      }
    }else if(typeof(obj) === 'number'){
      if(this.pattern.test(obj.toString())){
        return this.onOk();
      }else{
        return this.onError();
      }
    }else{
      return this.onOk();
    }
  };

  /******************************************************************************************/

  /**
   * Required rule
   */
  const EmailRule = function(){
    CommonRule.call(this);

    this.email = undefined;
    this.message = 'Validation failed: Invalid email address';
  };

  /** Inheritance */
  EmailRule.prototype = Object.create(CommonRule.prototype);
  EmailRule.prototype.contructor = EmailRule;

  /**
   * @param options
   */
  EmailRule.parse = function(options){
    var ins = new EmailRule();

    ins.init(options);

    if(typeof options === 'boolean'){
      // Email
      ins.email = options;
    }else if(Util.isNotEmptyString(options)){
      // Email
      ins.email   = true;
      // Message
      ins.message = options;
    }else if(Util.isNotNullObject(options)){
      // Email
      ins.email = true;

      if(Util.isNotEmptyString(options.message)){
        // Message
        ins.message = options.message;
      }
    }

    if(ins.email === undefined){
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`);
    }

    return ins;
  };

  /**
   *
   */
  EmailRule.prototype.getMessage = function(){
    return this.message;
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  EmailRule.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(! this.email){
      return this.onOk();
    }
    if(typeof obj !== 'string'){
      return this.onOk();
    }
    // http://emailregex.com/
    // /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/ // w3c
    let regx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(regx.test(obj)){
      return this.onOk();
    }

    return this.onError();
  };

  /******************************************************************************************/

  /**
   * Required rule
   */
  const LogicRule = function(){
    CommonRule.call(this);

    this.logic = undefined;
    this.context = undefined;
    this.message = 'Validation failed: Logic failed';
  };

  /** Inheritance */
  LogicRule.prototype = Object.create(CommonRule.prototype);
  LogicRule.prototype.contructor = LogicRule;

  /**
   * @param options
   */
  LogicRule.parse = function(options){
    var ins = new LogicRule();

    ins.init(options);

    if(typeof options === 'function'){
      // Logic
      ins.logic = options;
    }else if(Util.isNotNullObject(options)){
      // Logic
      if(typeof options.logic === 'function'){
        ins.logic = options.logic;
      }
      // Context
      if(Util.isNotNullObject(options.context)){
        ins.context = options.context;
      }
      // Message
      if(Util.isNotEmptyString(options.message)){
        ins.message = options.message;
      }
    }

    if(ins.logic === undefined){
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`);
    }

    return ins;
  };

  /**
   *
   */
  LogicRule.prototype.getMessage = function(){
    return this.message;
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  LogicRule.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    let rs = this.logic.call(this.context, obj);
    if(typeof(rs) === 'boolean' && rs === true){
      return this.onOk();
    }

    return this.onError();
  };

  /******************************************************************************************/

  /**
   * ValueRule class
   */
  const ChildRule = function(){
    this.rulechains = {};

    this.message        = undefined;
    this.subvalidateid  = undefined;
    this.subvalidateobj = undefined;
  }
  
  /** Prototype */
  ChildRule.prototype = Object.create(CommonRule.prototype);
  ChildRule.prototype.constructor = ChildRule;

  /**
   * @param options
   */
  ChildRule.parse = function(options){
    var ins = new ChildRule();

    // ins.init(options);

    if(Util.isNotNullObject(options)){
      for(var name in options){
        ins.rulechains[name] = RuleChain.parse(options[name]);
      };
    }else{
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 
    }

    return ins;
  };

  ChildRule.prototype.getMessage = function(){
    return this.message;
  };

  ChildRule.prototype.getValidateId = function(){
    return this.subvalidateid;
  };

  ChildRule.prototype.getValidateObj = function(){
    return this.subvalidateobj;
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  ChildRule.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(Util.isNotNullObject(obj)){
      let errflg = false;

      for(var name in this.rulechains){
        let rulechain = this.rulechains[name];
        let subobj    = obj[name];

        let subid = id ? `${id}.${name}` : name;
        if(! rulechain.validate(subobj, subid)){
          errflg = true;

          let errors = rulechain.getErrors();
          for(var error of errors){

            this.message        = error.message;
            this.subvalidateid  = error.id;
            this.subvalidateobj = error.value;

            this.onError();
          }

          break;
        }
      }

      this.message        = undefined;
      this.subvalidateid  = undefined;
      this.subvalidateobj = undefined;

      return errflg ? false : this.onOk();
    }else{
      this.message        = undefined;
      this.subvalidateid  = undefined;
      this.subvalidateobj = undefined;

      return this.onOk();
    }

  };

  /******************************************************************************************/

  /**
   * ValueRule class
   */
  const ChildrenRule = function(){
    this.rulechain = undefined;

    this.message        = undefined;
    this.subvalidateid  = undefined;
    this.subvalidateobj = undefined;
  }
  
  /** Prototype */
  ChildrenRule.prototype = Object.create(CommonRule.prototype);
  ChildrenRule.prototype.constructor = ChildrenRule;

  /**
   * @param options
   */
  ChildrenRule.parse = function(options){
    const RuleChain = require(`${__dirname}/../rule-chain.js`);

    var ins = new ChildrenRule();

    // ins.init(options);

    if(Util.isNotNullObject(options)){
      ins.rulechain = RuleChain.parse(options);
    }else{
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 
    }

    return ins;
  };

  ChildrenRule.prototype.getMessage = function(){
    return this.message;
  };

  ChildrenRule.prototype.getValidateId = function(){
    return this.subvalidateid;
  };

  ChildrenRule.prototype.getValidateObj = function(){
    return this.subvalidateobj;
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  ChildrenRule.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(Util.isNotNullObject(obj)){
      let errflg = false;

      for(var key in obj){
        let subobj = obj[key];
        let subid  = id ? `${id}.${key}` : key;

        this.rulechain.clearErrors();
        if(! this.rulechain.validate(subobj, subid)){
          errflg = true;

          let errors = this.rulechain.getErrors();
          for(var error of errors){

            this.message        = error.message;
            this.subvalidateid  = error.id;
            this.subvalidateobj = error.value;

            this.onError();
          }
        }

        break;
      }

      this.message        = undefined;
      this.subvalidateid  = undefined;
      this.subvalidateobj = undefined;

      return errflg ? false : this.onOk();
    }else{
      this.message        = undefined;
      this.subvalidateid  = undefined;
      this.subvalidateobj = undefined;

      return this.onOk();
    }

  };

  /******************************************************************************************/

  /**
   * RuleChain class
   */
  const RuleChain = function(){
    this.rules = [];
  };

  /**
   * @param options
   * @return RuleChain object
   */
  RuleChain.parse = function(options){
    if(! Util.isNotNullObject(options)) throw new Error('Invalid validation options: ' + JSON.stringify(options, null, 4));

    var ins = new RuleChain();

    for(var name in options){
      var rule = null;

      switch(name){
        case 'required':
          rule = RequiredRule.parse(options[name]);
        break;
        case 'typeof':
          rule = TypeofRule.parse(options[name]);
        break;
        case 'length':
          rule = LengthRule.parse(options[name]);
        break;
        case 'value':
          rule = ValueRule.parse(options[name]);
        break;
        case 'match':
          rule = MatchRule.parse(options[name]);
        break;
        case 'email':
          rule = EmailRule.parse(options[name]);
        break;
        case 'logic':
          rule = LogicRule.parse(options[name]);
        break;
        case 'child':
          rule = ChildRule.parse(options[name]);
        break;
        case 'children':
          rule = ChildrenRule.parse(options[name]);
        break;
      }

      if(! rule) throw new Error('Invalid validation rule: ' + name);

      ins.addRule(rule);
    }

    if(! Util.isNotEmptyArray(ins.rules)) throw new Error('Invalid validation options: ' + JSON.stringify(options, null, 4));

    return ins;
  };

  /**
   * @param id
   * @oaram obj
   * @param message
   */
  RuleChain.prototype.addError = function(id, obj, message){
    if(! this.errors){
      this.errors = [{
        id     : id,
        value  : obj,
        message: message
      }];
    }else{
      this.errors.push({
        id     : id,
        value  : obj,
        message: message
      });
    }
  };

  /**
   *
   */
  RuleChain.prototype.clearErrors = function(){
    this.errors = [];
  };

  /**
   *
   */
  RuleChain.prototype.getErrors = function(){
    return this.errors ? this.errors : [];
  };

  /**
   * @param rule
   */
  RuleChain.prototype.addRule = function(rule){
    if(rule && (rule instanceof CommonRule)){
      if(! this.rules){
        this.rules = [rule];
      }else{
        this.rules.push(rule);
      }
    }
  };

  /**
   * @param obj
   * @param id
   */
  RuleChain.prototype.validate = function(obj, id){
    this.clearErrors();

    if(! this.rules){
      return true;
    }

    for(var rule of this.rules){
      if(! rule.validate(obj, id, (id, obj, message)=>{
          this.addError(id, obj, message);
        }, null, this)){

        return false;
      }
    }

    return true;
  };

  /******************************************************************************************/
  
  /**
   * Class NodeObjValidator
   * Constructor
   * @param rules object Validation rules
   */
  const Validator = function(options){
    this.rulechain = RuleChain.parse(options);
  };

  /**
   * Do validate the given object
   * @param obj mixed Destination object to be validated
   * @param options object (optional) Options of this validation
   * @param onError
   * @param onOk
   * @return boolean
   */
  Validator.prototype.validate = function(obj, onError, onOk, context){
    if(! this.rulechain.validate(obj)){
      if(typeof onError === 'function'){
        onError.call(context, this.rulechain.getErrors());
      }

      return false;
    }else{
      if(typeof onOk === 'function'){
        onOk.call(context);
      }

      return true;
    }
  };

  // Utilities functions
  Validator.Util = Util;

  // Return class NodeObjValidatorn
  return Validator;

})());
